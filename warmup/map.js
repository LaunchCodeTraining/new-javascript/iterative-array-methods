const assert = require("assert");

const meanGobbos = [
  { mean: true },
  { mean: true },
  { mean: true },
  { mean: true },
];

const nicifyGobbo = (gobbo) => {
  // cast a spell that transforms each mean gobbo into a nice one!
};

const nicifiedGobbos = meanGobbos.map(nicifyGobbo);

// -- DO NOT EDIT BELOW -- //

try {
  assert.deepStrictEqual(
    nicifiedGobbos,
    [{ mean: false }, { mean: false }, { mean: false }, { mean: false }],
    "you have cast the wrong spell, help those mean gobbos see the nice side!"
  );
} catch (error) {
  console.log(error.message);
}
