// DO NOT EDIT THIS FILE
module.exports = [
  {
    username: "Ima38",
    avatarURL:
      "https://s3.amazonaws.com/uifaces/faces/twitter/sovesove/128.jpg",
    isFamily: true,
    isCloseFriend: true,
  },
  {
    username: "Rodrick_Schuster58",
    avatarURL: "https://s3.amazonaws.com/uifaces/faces/twitter/alagoon/128.jpg",
    isFamily: true,
    isCloseFriend: false,
  },
  {
    username: "Kaela_Reinger93",
    avatarURL:
      "https://s3.amazonaws.com/uifaces/faces/twitter/ismail_biltagi/128.jpg",
    isFamily: false,
    isCloseFriend: false,
  },
  {
    username: "Jody_Becker83",
    avatarURL:
      "https://s3.amazonaws.com/uifaces/faces/twitter/paulfarino/128.jpg",
    isFamily: true,
    isCloseFriend: false,
  },
  {
    username: "Verner71",
    avatarURL:
      "https://s3.amazonaws.com/uifaces/faces/twitter/madshensel/128.jpg",
    isFamily: true,
    isCloseFriend: false,
  },
  {
    username: "Landen.Rippin32",
    avatarURL:
      "https://s3.amazonaws.com/uifaces/faces/twitter/theonlyzeke/128.jpg",
    isFamily: true,
    isCloseFriend: true,
  },
  {
    username: "Crawford.Rau",
    avatarURL: "https://s3.amazonaws.com/uifaces/faces/twitter/artvavs/128.jpg",
    isFamily: false,
    isCloseFriend: false,
  },
  {
    username: "Brenna11",
    avatarURL: "https://s3.amazonaws.com/uifaces/faces/twitter/bfrohs/128.jpg",
    isFamily: false,
    isCloseFriend: true,
  },
  {
    username: "Guido.Carroll69",
    avatarURL:
      "https://s3.amazonaws.com/uifaces/faces/twitter/jeffgolenski/128.jpg",
    isFamily: false,
    isCloseFriend: false,
  },
  {
    username: "Santiago_Hills",
    avatarURL: "https://s3.amazonaws.com/uifaces/faces/twitter/toddrew/128.jpg",
    isFamily: false,
    isCloseFriend: true,
  },
];
