const assert = require("assert");

const friends = require("./friends");

const family = friends;
const closeFriends = friends;
const acquaintances = friends;

// -- DO NOT EDIT BELOW -- //

try {
  assert.deepStrictEqual(
    family,
    [
      {
        username: "Ima38",
        avatarURL:
          "https://s3.amazonaws.com/uifaces/faces/twitter/sovesove/128.jpg",
        isFamily: true,
        isCloseFriend: true,
      },
      {
        username: "Rodrick_Schuster58",
        avatarURL:
          "https://s3.amazonaws.com/uifaces/faces/twitter/alagoon/128.jpg",
        isFamily: true,
        isCloseFriend: false,
      },
      {
        username: "Jody_Becker83",
        avatarURL:
          "https://s3.amazonaws.com/uifaces/faces/twitter/paulfarino/128.jpg",
        isFamily: true,
        isCloseFriend: false,
      },
      {
        username: "Verner71",
        avatarURL:
          "https://s3.amazonaws.com/uifaces/faces/twitter/madshensel/128.jpg",
        isFamily: true,
        isCloseFriend: false,
      },
      {
        username: "Landen.Rippin32",
        avatarURL:
          "https://s3.amazonaws.com/uifaces/faces/twitter/theonlyzeke/128.jpg",
        isFamily: true,
        isCloseFriend: true,
      },
    ],
    "the family array contains non-family friends"
  );

  assert.deepStrictEqual(
    closeFriends,
    [
      {
        username: "Ima38",
        avatarURL:
          "https://s3.amazonaws.com/uifaces/faces/twitter/sovesove/128.jpg",
        isFamily: true,
        isCloseFriend: true,
      },
      {
        username: "Landen.Rippin32",
        avatarURL:
          "https://s3.amazonaws.com/uifaces/faces/twitter/theonlyzeke/128.jpg",
        isFamily: true,
        isCloseFriend: true,
      },
      {
        username: "Brenna11",
        avatarURL:
          "https://s3.amazonaws.com/uifaces/faces/twitter/bfrohs/128.jpg",
        isFamily: false,
        isCloseFriend: true,
      },
      {
        username: "Santiago_Hills",
        avatarURL:
          "https://s3.amazonaws.com/uifaces/faces/twitter/toddrew/128.jpg",
        isFamily: false,
        isCloseFriend: true,
      },
    ],
    "the closeFriends array contains non-close friends"
  );

  assert.deepStrictEqual(
    acquaintances,
    [
      {
        username: "Kaela_Reinger93",
        avatarURL:
          "https://s3.amazonaws.com/uifaces/faces/twitter/ismail_biltagi/128.jpg",
        isFamily: false,
        isCloseFriend: false,
      },
      {
        username: "Crawford.Rau",
        avatarURL:
          "https://s3.amazonaws.com/uifaces/faces/twitter/artvavs/128.jpg",
        isFamily: false,
        isCloseFriend: false,
      },
      {
        username: "Guido.Carroll69",
        avatarURL:
          "https://s3.amazonaws.com/uifaces/faces/twitter/jeffgolenski/128.jpg",
        isFamily: false,
        isCloseFriend: false,
      },
    ],
    "the acquaintances array contains family and/or close friends"
  );
} catch (error) {
  console.log(error.message);
}
