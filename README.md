# Iterative Array Methods Practice

For each practice problem:

1. open the respective starter code
2. implement the solution
3. execute the script

If no error message is printed you have implemented correctly.

# Warmup: Gobbos!

```js
const gobbos = [
  { mean: true },
  { mean: false },
  { mean: false },
  { mean: false },
  { mean: false },
  { mean: true },
  { mean: false },
  { mean: false },
  { mean: false },
  { mean: false },
  { mean: false },
  { mean: true },
  { mean: true },
];
```

## Warmup 1 `filter()`

> Starter code is available in `warmup/filter.js`.

You have a small gathering of goblins gathered at your feet. You don't mind the nice ones but the mean ones are rude and keep nipping at your toes. You have to separate the gobbos into two groups, nice and mean. The nice ones are cute and get to stay but the mean ones get punted into the swamp!

Fortunately (unfortunately?) you don't have real gobbos at your toes. You have been given an initial list of `gobbos` with a mix of nice and mean. Your challenge is to separate the list into two new lists: `niceGobbos` and `meanGobbos`.

## Warmup 2 `map()`

> Starter code is available in `warmup/map.js`

A powerful wizard has frozen you mid-punt of a cowering mean gobbo. The wizard offers to teach you a spell that will transform the mean gobbos into nice ones. This should be easy since you have already separated them.

Your challenge is to write the spell! Fill in the `nicifyGobbo` callback function use it to transform all the `meanGobbos`.

# Real World: Frontend Development

## Real World 1 `filter()`

You are designing a component for a user profile on your frontend. The component needs to show a list of friends separated by three sections: Family, Close Friends, and Other Friends. You receive an Array of `friends` from an API request with the following shape:

```js
{
  username: String,
  avatarURL: String,
  isFamily: Boolean,
  isCloseFriend: Boolean,
}
```

Your challenge is to separate the list of friends into three Arrays according to the rules below:

- `family`: only friends that have a `isFamily` property of `true`
- `closeFriends`: only friends that have a `isCloseFriend` property of `true`
- `acquaintances`: all other friends that are neither family nor close

```js
const friends = [
  {
    username: "Ima38",
    avatarURL:
      "https://s3.amazonaws.com/uifaces/faces/twitter/sovesove/128.jpg",
    isFamily: true,
    isCloseFriend: true,
  },
  {
    username: "Rodrick_Schuster58",
    avatarURL: "https://s3.amazonaws.com/uifaces/faces/twitter/alagoon/128.jpg",
    isFamily: true,
    isCloseFriend: false,
  },
  {
    username: "Kaela_Reinger93",
    avatarURL:
      "https://s3.amazonaws.com/uifaces/faces/twitter/ismail_biltagi/128.jpg",
    isFamily: false,
    isCloseFriend: false,
  },
  {
    username: "Jody_Becker83",
    avatarURL:
      "https://s3.amazonaws.com/uifaces/faces/twitter/paulfarino/128.jpg",
    isFamily: true,
    isCloseFriend: false,
  },
  {
    username: "Verner71",
    avatarURL:
      "https://s3.amazonaws.com/uifaces/faces/twitter/madshensel/128.jpg",
    isFamily: true,
    isCloseFriend: false,
  },
  {
    username: "Landen.Rippin32",
    avatarURL:
      "https://s3.amazonaws.com/uifaces/faces/twitter/theonlyzeke/128.jpg",
    isFamily: true,
    isCloseFriend: true,
  },
  {
    username: "Crawford.Rau",
    avatarURL: "https://s3.amazonaws.com/uifaces/faces/twitter/artvavs/128.jpg",
    isFamily: false,
    isCloseFriend: false,
  },
  {
    username: "Brenna11",
    avatarURL: "https://s3.amazonaws.com/uifaces/faces/twitter/bfrohs/128.jpg",
    isFamily: false,
    isCloseFriend: true,
  },
  {
    username: "Guido.Carroll69",
    avatarURL:
      "https://s3.amazonaws.com/uifaces/faces/twitter/jeffgolenski/128.jpg",
    isFamily: false,
    isCloseFriend: false,
  },
  {
    username: "Santiago_Hills",
    avatarURL: "https://s3.amazonaws.com/uifaces/faces/twitter/toddrew/128.jpg",
    isFamily: false,
    isCloseFriend: true,
  },
];
```

## Real World 2 `map()`

Your user base and their friends lists are growing rapidly. You have decided to refactor the friends component to show more users in the same space. You plan on accomplishing this with just showing their usernames and dropping the avatar images. You receive the same list of friends from your API.

Your challenge is to transform the `friends` list into a list of just the friend usernames, like:

```js
["Guido.Carroll69", "Santiago_Hills", ...]
```
