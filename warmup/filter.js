const assert = require("assert");

const gobbos = [
  { mean: true },
  { mean: false },
  { mean: false },
  { mean: false },
  { mean: false },
  { mean: true },
  { mean: false },
  { mean: false },
  { mean: false },
  { mean: false },
  { mean: false },
  { mean: true },
  { mean: true },
];

// filter the gobbos into separate piles
const niceGobbos = gobbos;
const meanGobbos = gobbos;

// -- DO NOT MODIFY BELOW -- //

try {
  assert.deepStrictEqual(
    niceGobbos,
    [
      { mean: false },
      { mean: false },
      { mean: false },
      { mean: false },
      { mean: false },
      { mean: false },
      { mean: false },
      { mean: false },
      { mean: false },
    ],
    "there are still mean gobbos in the nice gobbos pile!"
  );

  assert.deepStrictEqual(
    meanGobbos,
    [{ mean: true }, { mean: true }, { mean: true }, { mean: true }],
    "there are still nice gobbos in the mean gobbos pile!"
  );
} catch (error) {
  console.log(error.message);
}

module.exports = {
  gobbos,
  niceGobbos,
  meanGobbos,
};
