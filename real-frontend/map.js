const assert = require("assert");
const friends = require("./friends");

const usernames = friends;

// -- DO NOT EDIT BELOW -- //

try {
  assert.deepStrictEqual(
    usernames,
    [
      "Ima38",
      "Rodrick_Schuster58",
      "Kaela_Reinger93",
      "Jody_Becker83",
      "Verner71",
      "Landen.Rippin32",
      "Crawford.Rau",
      "Brenna11",
      "Guido.Carroll69",
      "Santiago_Hills",
    ],
    "the array should only contain username elements"
  );
} catch (error) {
  console.log(error.message);
}
